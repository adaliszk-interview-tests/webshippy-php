<!DOCTYPE html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>Webshippy Teszt</title>
    <style>
        body, td, th {
            font-family: Roboto, Arial, sans-serif;
        }
        body, html {
            margin: 0; padding: 0;
            min-height: 100vh;
        }
        body {
            background: #ddd;
            padding-left: 220px;
        }
        #data {
            width: 100%; max-width: 960px;
            margin: 0 auto;
            background: #fafafa;
        }
        #new_data {
            position: fixed; left: 0; top: 0; bottom: 0; width; 200px;
            background: #222; color: #eee;
        }
        #new_data h1 {
            text-align: center;
        }
        #new_data fieldset {
            display: flex; flex-direction: column;
            border: 1px solid #555;
            padding: 10px; margin: 10px;
        }
        #new_data input {
            padding: 10px; border: none;
            margin: 5px 0;
        }
        #new_data div {
            padding: 10px;
        }
        #new_data label {
            position: relative; top: -2px;
            padding: 10px;
        }
        #new_data button {
            float: right;
            padding: 10px; margin: 10px;
            background: dodgerblue;
            border: none;
        }
    </style>
</head>
<body>
    <aside id="new_data">
        <h1>Új adatok felvétele</h1>
        <form id="form" method="POST" action="api.php">
            <input type="hidden" name="id" value="" />
            <fieldset id="altalanos">
                <legend>Általános adatok</legend>
                <input type="text" name="name" autocomplete="name" placeholder="Név" />
                <input type="email" name="email" autocomplete="email" placeholder="Email" />
                <input type="tel" name="phone" autocomplete="tel" placeholder="Telefonszám" />
                <input type="date" name="birthday" placeholder="Születési dátum" />
            </fieldset>
            <fieldset id="jogositvany">
                <legend>Jogsítvány?</legend>
                <?php foreach (['van', 'nincs'] as $idx => $label): ?>
                    <label for="jogsi_<?=$idx?>">
                        <input type="radio" id="jogsi_<?=$idx?>" name="driverLicense" value="<?=$label?>">
                        <?=$label?>
                    </label>
                <?php endforeach; ?>
            </fieldset>
            <fieldset id="hobbik">
                <legend>Hobbik?</legend>
                <?php foreach (['kerékpározás', 'túrázás', 'hegymászás', 'programozás', 'egyéb'] as $idx => $hobbi): ?>
                    <label for="hobbi_<?=$idx?>">
                        <input type="checkbox" id="hobbi_<?=$idx?>" name="hobby" value="<?=$hobbi?>">
                        <?=$hobbi?>
                    </label>
                <?php endforeach; ?>
            </fieldset>
            <button type="submit">
                Küldés
            </button>
        </form>
    </aside>
    <main>
        <table id="data">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Név</th>
                    <th>Email</th>
                    <th>Telefonszám</th>
                    <th>Születés</th>
                    <th>Jogsi?</th>
                    <th>Hobbik?</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody id="data_body">

            </tbody>
        </table>
        <template id="data_row" style="display: none;">
            <tr>
                <td class="data" data-attr="id">${item.id}</td>
                <td class="data" data-attr="name">${item.name}</td>
                <td class="data" data-attr="email">${item.email}</td>
                <td class="data" data-attr="phone">${item.phone}</td>
                <td class="data" data-attr="birthday">${item.birthday}</td>
                <td class="data" data-attr="driverLicense">${item.driverLicense}</td>
                <td class="data" data-attr="hobby">${item.hobby}</td>
                <td><button class="edit">Szerkeszt️</button></td>
            </tr>
        </template>
    </main>
    <!--suppress JSCheckFunctionSignatures -->
    <script type="module">
        const form = document.getElementById('form');
        const dataRowTemplate = document.getElementById('data_row');
        const dataContainer = document.getElementById('data_body');
        window.data = {};

        update_table();

        // Form küldésének felülírása
        form.onsubmit = event => {
            event.preventDefault();

            let jsonData = {};
            (new FormData(form)).forEach((value, key) => {
                if (typeof jsonData[key] !== 'undefined') {
                    if (!(jsonData[key] instanceof Array)) jsonData[key] = [jsonData[key]];
                    jsonData[key].push(value);
                } else {
                    jsonData[key] = value;
                }
            });

            let method = 'PUT';
            if (jsonData.id.length > 0) method = 'PATCH';

            fetch('/api.php/persons', {method: method, body: JSON.stringify(jsonData)})
                .then(response => response.json())
                .then(responseJson => {
                    console.log(responseJson);
                    if (responseJson.errors.length > 0) {
                        responseJson.errors.forEach(err => {
                            alert(`Hiba történt!\n ${err.detail}`);
                        });
                    } else {
                        update_table();
                        form.reset();
                    }
                });
        };

        function update_table() {
            fetch('/api.php/persons', {method: 'GET'})
                .then(response => response.json())
                .then(responseJson => {
                    if (responseJson.errors.length > 0) {
                        responseJson.errors.forEach(err => {
                            alert(`Hiba történt!\n ${err.detail}`);
                        });
                    } else {
                        dataContainer.innerHTML = null;
                        window.data = {};

                        responseJson.data.forEach(item => {
                            window.data[item.id] = item;
                            const row = document.importNode(dataRowTemplate.content, true);
                            row.querySelectorAll('.data').forEach(/** @type HTMLElement */ slot => {
                               slot.innerText = item[slot.dataset.attr];
                            });
                            row.querySelector('button.edit').addEventListener('click', edit_item);
                            dataContainer.appendChild(row);
                        })
                    }
                });
        }

        function edit_item(/** @type Event */ event) {
            const row = event.target.parentElement.parentElement;
            const id = row.querySelector('.data[data-attr="id"]').innerText;
            const data = window.data[id];

            form.reset();
            form.querySelectorAll('input').forEach(/** @type HTMLInputElement */ input => {
                const value = data[input.name];
                // Checkbox, Radio
               if (input.value.length > 0) {
                    if (value.includes(input.value)) {
                        input.checked = true;
                    }
               } else {
                   input.value = value;
               }
            });

            debugger;
        }
    </script>
</body>
</html>
