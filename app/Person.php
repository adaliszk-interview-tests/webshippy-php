<?php
namespace WebShippy\PersonManager;

use DateTime;
use JsonSerializable;
use Throwable;

/**
 * Value-Object Person
 *
 * @package WebShippy\PersonManager
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property DateTime $birthday
 * @property string $driverLicense
 * @property string[] $hobby
 */
class Person implements JsonSerializable
{
    const FIELDS = ['name', 'email', 'phone', 'birthday', 'driverLicense', 'hobby'];

    protected $id;
    protected $name;
    protected $email;
    protected $phone;
    protected $birthday;
    protected $driverLicense;
    protected $hobby;

    // @TODO: Rendes hitelesítés
    public function __construct(
        int $id = null,
        string $name,
        string $email,
        string $phone,
        string $birthday,
        string $driverLicense = 'N/A',
        $hobby = ''
    ) {
        // Adatok betöltése
        foreach (self::FIELDS as $field) {
            // Normál esetben minden paraméterre lenne egy formázó metódus így gyorsabb a lefutás ha azt feltételezzük,
            // hogy van is egy ilyen metódus
            try {
                $format_method = 'format' . ucfirst($field);
                $this->$field = $this->$format_method($$field);
            } catch (Throwable $e) {
                // @TODO: Ahelyett, hogy megőrizzük a formázatlan, ellenőrizetlen bemenetet, dobjunk egy hibát
                $this->$field = $$field;
            }
        }

        // Formázott vagy komplex mezők
        if (!empty($id)) $this->id = $id;
    }

    public function formatBirthday(string $value): DateTime
    {
        return new DateTime($value);
    }

    public function formatHobby($value): array
    {
        if (is_array($value)) return $value;
        return explode(',', $value);
    }

    // @TODO: Esetleges jogosultság ellenőrzés
    public function __get($name)
    {
        try {
            $getter_method = 'get' . ucfirst($name);
            return $this->$getter_method();
        } catch (Throwable $e) {
            // @TODO: Dobjunk hibát ha hiányzik
            return $this->$name;
        }
    }

    public function getBirthday()
    {
        return $this->birthday->format('Y-m-d');
    }

    public function getHobby()
    {
        // @TODO: Rendes serialize ehleyett
        return implode(',', $this->hobby);
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        $data = ['id' => $this->id];
        foreach (self::FIELDS as $attr_name) {
            try {
                $getter_method = 'get' . ucfirst($attr_name);
                $data[$attr_name] = $this->$getter_method();
            } catch (Throwable $e) {
                // @TODO: Dobjunk hibát ha hiányzik
                $data[$attr_name] = $this->$attr_name;
            }
        }
        return $data;
    }
}
