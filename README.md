WebShippy PHP Teszt
===================

1. Készíts el egy űrlapot ami a következő adatokat kéri be:
 - Név
 - Email
 - Telefonszám
 - Születés dátuma
 - Van e jogosítványa(radio)(igen|nem)
 - Kedvenc hobbi(checkbox, vagy multiple select) (kerékpározás|túrázás|hegymászás|programozás|egyéb)

2. Készítsd el az adatbázist, és mentsd el az űrlap tartalmát  AJAX hívás segítségével (visszajelzés a mentés eredményéről).

3. Készíts el egy listanézetet ahol az eddig elmentett adatokat kilistázod AJAX segítségével.
(esetleg a jelentkező nevére lehessen szűrni is, és/vagy sorbarendezni)

4. A listába kell egy szerkesztés gomb ami az űrlapra navigál ahol lehet módosítani a korábban rögzített adatokat.

A kliens, és a backend oldalán is igyekezz objektum orientált megoldásokat alkalmazni.
A kliens, és a backend oldalán a külső pluginok, és keretrendszerek használata a jQuery kivételével nem engedélyezettek.
Az adatbázis kezelésre kérlek használj \PDO-t.


Használat
=========

Projekt futtatása előtt migrációra van szükség, ezt a composer-en keresztül lehet megvalósítani:
```
composer run migration
```

A fejlesztéshez nincs szükség semmi másra mint egy 7-es PHP-ra és az sqlite kiegészítőre!
Ahhoz, hogy fejlesszünk futtatni kell a php beépített szerverét:
```
composer run server
```
